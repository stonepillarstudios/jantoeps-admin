package com.sps.janadmin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        JanTools.setContext(this);
        new GetUpdates().execute();
    }
    private boolean bWeird;
    private boolean bVakansie;
    private boolean bEksamen;
    private int iVersion;
    private static final String url_update_updates = "http://apps.rauten.co.za/jantoeps/run.php";
    private static final String url_get_updates = "http://apps.rauten.co.za/jantoeps/get_all_updates.php";

    public void onAfkondigingsClick(View view) {
        gotoActivity(Afkondigings.class);
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    private ProgressDialog dialog;

    public void onUploadClick(View view) {

        bWeird = ((ToggleButton) findViewById(R.id.tglWeird)).isChecked();
        bVakansie = ((ToggleButton) findViewById(R.id.tglVakansie)).isChecked();
        bEksamen = ((ToggleButton) findViewById(R.id.tglEksamen)).isChecked();
        iVersion = Integer.parseInt(((EditText) findViewById(R.id.edtVersion)).getText().toString());

        new UpdateUpdates().execute("");
    }

    public void onVersionUpClick(View view) {
        ((EditText) findViewById(R.id.edtVersion)).setText(
                ((Integer.parseInt(
                        ((EditText) findViewById(R.id.edtVersion)).getText().toString()))
                        +1)
                        +""
        );
        //enemy of the state
    }

    class UpdateUpdates extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainScreen.this);
            dialog.setMessage("Updating updates...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            JSONObject json = new JSONObject();

            String[][] nameVals = {
                    {"weird_tye", bWeird+""},
                    {"vakansie", bVakansie+""},
                    {"eksamen", bEksamen+""},
                    {"latest_version", iVersion+""}
            };

            try {

                for (String[] nameVal : nameVals) {

                    json.put("query", "UPDATE updates SET state = '"+ nameVal[1] +"' WHERE name = '"+ nameVal[0] +"'");

                    HttpURLConnection httpCon = (HttpURLConnection) new URL(url_update_updates).openConnection();
                    httpCon.setRequestMethod("POST");
                    httpCon.setConnectTimeout(10_000);
                    httpCon.setReadTimeout(10_000);
                    httpCon.setRequestProperty("Content-Type", "application/json");
                    httpCon.setRequestProperty("Accept", "application/json");
                    httpCon.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(httpCon.getOutputStream());
                    writer.write(json.toString());
                    writer.flush();
                    writer.close();

                    int result = httpCon.getResponseCode();

                    StringBuilder sb = new StringBuilder();
                    if (result == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                httpCon.getInputStream(), "utf-8"));

                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                        br.close();

                        final String output = sb.toString();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.txtOutput)).setText(
                                        ((TextView) findViewById(R.id.txtOutput)).getText().toString()
                                                + output); //show response: ok
                            }
                        });
                        System.out.print("RESULT: " + output);

                    } else {
                        final String output = httpCon.getResponseMessage();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.txtOutput)).setText(
                                        ((TextView) findViewById(R.id.txtOutput)).getText().toString()
                                                + output); //show response: fail
                            }
                        });
                        System.out.print(output);
                    }
                }

            } catch (JSONException|IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            //super.onPostExecute(s);
            dialog.dismiss();
        }
    }


    class GetUpdates extends AsyncTask<Void, String, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainScreen.this);
            dialog.setMessage("Getting updates...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... v) { //todo other bools

            try {

                HttpURLConnection httpCon = (HttpURLConnection) new URL(url_get_updates).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    System.out.println("RESPONSE = " + sb.toString());

                    return new JSONObject(sb.toString());

                } else {
                    JanTools.makeToast("HTTP Error");
                }

            } catch (JSONException|IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(final JSONObject json) {
            //super.onPostExecute(s);

            if (json == null){
                JanTools.makeToast("Error getting update");
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        postCustom(json);
                    }
                });
            }
        }

        private void postCustom(JSONObject json){

            try {
                JSONArray jarr = json.getJSONArray("updates");

                String[][] updates = new String[jarr.length()][2];

                for (int i = 0; i < updates.length; i++){
                    updates[i][0] = ((JSONObject) jarr.get(i)).getString("name");
                    updates[i][1] = ((JSONObject) jarr.get(i)).getString("state");
                }

                if (getState(updates, "weird_tye").equalsIgnoreCase("true"))
                    ((ToggleButton) findViewById(R.id.tglWeird)).toggle();
                if (getState(updates, "eksamen").equalsIgnoreCase("true"))
                    ((ToggleButton) findViewById(R.id.tglEksamen)).toggle();
                if (getState(updates, "vakansie").equalsIgnoreCase("true"))
                    ((ToggleButton) findViewById(R.id.tglVakansie)).toggle();
                ((EditText) findViewById(R.id.edtVersion)).setText(getState(updates, "latest_version"));



            } catch (JSONException|NullPointerException e) {
                e.printStackTrace();
            } finally {
                dialog.dismiss();
            }
        }

        private String getState(String[][] arr, String name){
            for (String[] anArr : arr) {
                if (anArr[0].equals(name))
                    return anArr[1];
            }
            return ""; //shouldn't ever happen
        }
    }
}


