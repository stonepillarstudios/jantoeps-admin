package com.sps.janadmin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class Afkondigings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afkondigings);
        JanTools.setContext(this);
        ((TextView) findViewById(R.id.txtOutput)).setMovementMethod(new ScrollingMovementMethod());
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private static final String url_run = "http://apps.rauten.co.za/jantoeps/run.php";

    public void onBackClick(View view) {
        gotoActivity(MainScreen.class);
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    private String f2(int i){ //force 2
        return (i+"").length()==1 ? "0"+i : i+""; //code gholf, bitch.
    }

    public void onSlashClick(View view) {
        EditText edtAfk = ((EditText) findViewById(R.id.edtAfkondigings));
        String curr = edtAfk.getText().toString();

        DateTime dt = new DateTime(new Date());

        String dateNum = dt.getYear() +"-"+ f2(dt.getMonthOfYear()) +"-"+ f2(dt.getDayOfMonth());
        curr += "\\" + dateNum;

        edtAfk.setText(curr);

        ((EditText) findViewById(R.id.edtAfkondigings)).setSelection(((EditText) findViewById(R.id.edtAfkondigings)).getText().toString().length()-1);
    }

    public void onSemicolonClick(View view) {
        ((EditText) findViewById(R.id.edtAfkondigings)).setText(
                ((EditText) findViewById(R.id.edtAfkondigings)).getText().toString()
                + ";"
        );
        ((EditText) findViewById(R.id.edtAfkondigings)).setSelection(((EditText) findViewById(R.id.edtAfkondigings)).getText().toString().length()-1);
    }

    private String[][] afkData;

    public void onInsertClick(View view) {
        String raw = ((EditText) findViewById(R.id.edtAfkondigings)).getText().toString();
        String[] afks = raw.split(";");

        afkData = new String[afks.length][2];
        for (int i = 0; i < afks.length; i++){
            afkData[i][0] = afks[i].split("\\\\")[0]; //hey kinders!
            afkData[i][1] = afks[i].split("\\\\")[1]; //20160911
        }

        new InsertAfks().execute();

    }

    private ProgressDialog dialog;

    public void onViewAndDelClick(View view) {
        gotoActivity(ViewAndDel.class);
    }

    class InsertAfks extends AsyncTask<Void, Void, Void>{

        @Override
        public void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(Afkondigings.this);
            dialog.setMessage("Inserting new afkondigings...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        public Void doInBackground(Void... v){
            JSONObject json = new JSONObject();

            try {

                for (String[] afk : afkData) {

                    String afk_safe = afk[0].trim().replace("'", "''");
                    String date_safe = afk[1].trim().replace(" ", "");

                    json.put("query", "INSERT INTO afkondigings(afkondiging, expiry_date) VALUES('"+afk_safe+"', '"+date_safe+" 23:59:59')");
                    System.out.println("SENT: " + json.toString());

                    HttpURLConnection httpCon = (HttpURLConnection) new URL(url_run).openConnection();
                    httpCon.setRequestMethod("POST");
                    httpCon.setConnectTimeout(10_000);
                    httpCon.setReadTimeout(10_000);
                    httpCon.setRequestProperty("Content-Type", "application/json");
                    httpCon.setRequestProperty("Accept", "application/json");
                    httpCon.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(httpCon.getOutputStream());
                    writer.write(json.toString());
                    writer.flush();
                    writer.close();

                    int result = httpCon.getResponseCode();

                    StringBuilder sb = new StringBuilder();
                    if (result == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                httpCon.getInputStream(), "utf-8"));

                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append("\n");
                        }
                        br.close();

                        final String output = sb.toString();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.txtOutput)).setText(
                                        ((TextView) findViewById(R.id.txtOutput)).getText().toString()
                                                + output); //show response: ok
                            }
                        });
                        System.out.print("RESULT: " + output);

                    } else {
                        final String output = httpCon.getResponseMessage();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.txtOutput)).setText(
                                        ((TextView) findViewById(R.id.txtOutput)).getText().toString()
                                                + output); //show response: fail
                            }
                        });
                        System.out.print(output);
                    }
                }

            } catch (JSONException |IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        public void onPostExecute(Void v){
            dialog.dismiss();
        }

    }
}
